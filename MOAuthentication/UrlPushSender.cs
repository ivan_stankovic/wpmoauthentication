﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MOAuthentication
{
    public class UrlPushSender
    {
        public UrlPushSender()
        {

        }
        internal static string GetStringFromResponse( HttpWebResponse response)
        {
            string retV = "";
            if (response.ContentLength > 0)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    retV = sr.ReadToEnd();
                }
            }
            return retV;
        }
        internal static Task<HttpWebResponse> GetResponseAsync( HttpWebRequest request)
        {
            var taskCompletionSource = new TaskCompletionSource<HttpWebResponse>();
            try
            {
                request.BeginGetResponse(iar =>
                {
                    try
                    {
                        taskCompletionSource.SetResult((HttpWebResponse)request.EndGetResponse(iar));
                    }
                    catch (Exception inner_exception)
                    {
                        taskCompletionSource.SetException(inner_exception);
                    }
                }, null);
            }
            catch (Exception outher_exception)
            {
                taskCompletionSource.SetException(outher_exception);
            }
            return taskCompletionSource.Task;
        }

        internal static Task<Stream> GetRequestStreamAsync(HttpWebRequest request)
        {
            var taskCompletionSource = new TaskCompletionSource<Stream>();
            try
            {
                request.BeginGetRequestStream(asyncCallback =>
                {
                    try
                    {
                        taskCompletionSource.SetResult(request.EndGetRequestStream(asyncCallback));
                    }
                    catch (Exception inner_exception)
                    {
                        taskCompletionSource.SetException(inner_exception);
                    }
                }, null);
            }
            catch (Exception outher_exception)
            {
                taskCompletionSource.SetException(outher_exception);
            }
            return taskCompletionSource.Task;
        }
        public async Task<bool> Subscribe(string deviceId, string url)
        {
            HttpWebResponse response;
            try
            {
                var request = HttpWebRequest.CreateHttp("http://infobip.cer.co.rs/subscribe.php");
                request.Method = "POST";
                request.ContentType = "application/json";
                string json = "{\"device_id\":\"" + deviceId + "\",\"push_url\":" + url + "\"}";
                byte[] data = Encoding.UTF8.GetBytes(json);

                if (data != null)
                {
                    using (Stream requestStreamAsync = await GetRequestStreamAsync(request))
                    {
                        await requestStreamAsync.WriteAsync(data, 0, data.Length);
                    }
                }

                response = (HttpWebResponse)await GetResponseAsync(request);

            }

            catch (WebException e)
            {
                response = (HttpWebResponse)e.Response;

            }
            return response.StatusCode == HttpStatusCode.OK;

        }

    }

}
