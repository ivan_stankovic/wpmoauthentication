﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOAuthentication.Util
{
    public class Storage
    {
        private static string NAME_PREFIX = "B087941F-C295-474F-8DD8-C65DA74C54C4";
        private IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;

        private static Storage _instacne;
        private Storage()
        {
        }
        public static Storage GetInstance()
        {
            if (_instacne == null)
                _instacne = new Storage();
            return _instacne;
        }
        public object this[string key]
        {
            get
            {
                if (appSettings.Contains(NAME_PREFIX + key))
                {
                    object val;
                    appSettings.TryGetValue(NAME_PREFIX + key, out val);
                    if (val != null)
                        return val;
                    return appSettings[NAME_PREFIX + key];

                }
                return null;
            }
            set
            {
                if (appSettings.Contains(NAME_PREFIX + key))
                    appSettings[NAME_PREFIX + key] = value;
                else
                    appSettings.Add(NAME_PREFIX + key, value);
                appSettings.Save();
            }
        }

    }

    public class StorageVariables
    {
        public const string DEVICEID = "DEVICEID";
        public const string URL = "URL";
    }
}
