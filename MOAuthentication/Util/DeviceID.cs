﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOAuthentication.Util
{
    public class DeviceID
    {
        public static string GenerateDeviceID()
        {
            if (Storage.GetInstance()[StorageVariables.DEVICEID] == null)
            Storage.GetInstance()[StorageVariables.DEVICEID] =   "W"
                + DateTime.Now.Year % 100
                + DateTime.Now.Month.ToString("00")
                + DateTime.Now.Day.ToString("00")
                + DateTime.Now.Hour.ToString("00")
                + DateTime.Now.Minute.ToString("00")
                + DateTime.Now.Second.ToString("00")
                + DateTime.Now.Millisecond.ToString("000")
                + GenerateRandom4Letters();

            return Storage.GetInstance()[StorageVariables.DEVICEID].ToString();

        }
        private static string GenerateRandom4Letters()
        {
            string random4letters = "";
            Random r = new Random();


            //First letter
            if (r.Next(0, 200) > 100)
                random4letters += (char)r.Next(65, 90);
            else
                random4letters += (char)r.Next(97, 122);
            //Second letter
            if (r.Next(0, 200) > 100)
                random4letters += (char)r.Next(65, 90);
            else
                random4letters += (char)r.Next(97, 122);
            //Third letter
            if (r.Next(0, 200) > 100)
                random4letters += (char)r.Next(65, 90);
            else
                random4letters += (char)r.Next(97, 122);
            //Four letter
            if (r.Next(0, 200) > 100)
                random4letters += (char)r.Next(65, 90);
            else
                random4letters += (char)r.Next(97, 122);

            return random4letters;
        }
    }
}
