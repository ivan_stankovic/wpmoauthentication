﻿using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOAuthentication
{
    public class SMS
    {
        public static void SendSms(string number, string body)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();
            smsComposeTask.To = number;
            smsComposeTask.Body = body;
            smsComposeTask.Show();

        }
    }
}
