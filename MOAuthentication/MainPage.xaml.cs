﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MOAuthentication.Resources;
using Microsoft.Phone.Info;
using MOAuthentication.Util;

namespace MOAuthentication
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();


            if (Storage.GetInstance()[StorageVariables.DEVICEID]==null)
            {
                Storage.GetInstance()[StorageVariables.DEVICEID] = DeviceID.GenerateDeviceID();
            }
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private async void btnSendSMS_Click(object sender, RoutedEventArgs e)
        {
            UrlPushSender ups = new UrlPushSender();

            string deviceID = Storage.GetInstance()[StorageVariables.DEVICEID].ToString();  //DeviceExtendedProperties.GetValue("DeviceUniqueId").ToString();
            string chanelURL = Storage.GetInstance()[StorageVariables.URL].ToString();  //DeviceExtendedProperties.GetValue("DeviceUniqueId").ToString();

            bool isSubscribed = await ups.Subscribe(deviceID, chanelURL);
            if (isSubscribed)
            {

                SMS.SendSms("+385997894508", "dragan " + deviceID);
            }
            else
            {
                MessageBox.Show("Connection problem");
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            base.OnNavigatedTo(e);
            string parameter = string.Empty;
            if (NavigationContext.QueryString.TryGetValue("phonenumber", out parameter))
            {
                txtPhoneNumber.Text = parameter;
                btnSendSMS.IsEnabled = false;
            }
               
            
        }

       
        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}