﻿using Microsoft.Phone.Notification;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PushMO
{
    public class Push
    {
        private HttpNotificationChannel notificationChanel;
        public delegate void OnUrlCHangedHandler(string url);
        public event OnUrlCHangedHandler UrlChanged;
        public string ApiKey { get; private set; }
        public static event OnUrlCHangedHandler NumberPush;
        private static string ChannelID()
        {
            return "chanelid:" + "";
        }
        //public Push(string apiKey)
        //{
        //    this.ApiKey = apiKey;
        //}
        public void CreateANotificationChannel()
        {
            notificationChanel = HttpNotificationChannel.Find(ChannelID());

            if (notificationChanel == null)
            {
                notificationChanel = new HttpNotificationChannel(ChannelID());
                SetUpDelegates();

                notificationChanel.Open();
            }
            else
            {
                SetUpDelegates();
                if (notificationChanel.ChannelUri == null)
                {
                    // The notification channel URI has not been sent to the client. Wait for the ChannelUriUpdated delegate to fire.
                    // If your application requires a timeout for setting up a notification channel, start it here.
                }
                else
                {
                    DoOnUrlChange(notificationChanel.ChannelUri.ToString());
                    Debug.WriteLine("Notification channel URI:" + notificationChanel.ChannelUri.ToString());
                }

            }

            if (notificationChanel.ChannelUri != null)
            {
                SendURLChannelToServer(notificationChanel.ChannelUri.ToString());
            }



            notificationChanel.UnbindToShellToast();
            BindingANotificationsChannelToAToastNotification();
        }


        public void SetUpDelegates()
        {
            notificationChanel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(notificationChanel_ChannelUriUpdated);
            notificationChanel.HttpNotificationReceived += new EventHandler<HttpNotificationEventArgs>(notificationChanel_HttpNotificationReceived);
            notificationChanel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(notificationChanel_ShellToastNotificationReceived);
            notificationChanel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(notificationChanel_ErrorOccurred);
        }
        public void DoOnUrlChange(string url)
        {
            var ev = UrlChanged;
            if (ev != null)
                ev(url);
        }
        void notificationChanel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            // The URI that the application will send to its corresponding web service.
            Debug.WriteLine("Notification channel URI:" + e.ChannelUri.ToString());
                
            
            DoOnUrlChange(e.ChannelUri.ToString());

            // SendURIToService(e.ChannelUri);

        }
        private void SendURLChannelToServer(string url)
        {

            Debugger.Log(1, "Push", "Sending url to server  " + url);

        }
        void notificationChanel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            switch (e.ErrorType)
            {
                case ChannelErrorType.ChannelOpenFailed:
                    this.CreateANotificationChannel();
                    break;
                case ChannelErrorType.MessageBadContent:
                    this.CreateANotificationChannel();
                    break;
                case ChannelErrorType.NotificationRateTooHigh:
                    this.CreateANotificationChannel();
                    break;
                case ChannelErrorType.PayloadFormatError:
                    this.CreateANotificationChannel();
                    break;
                case ChannelErrorType.PowerLevelChanged:
                    this.CreateANotificationChannel();
                    break;
            }
        }

        // Receiving a toast notification. 
        // Toast notifications are only delivered to the device when the application is not running in the foreground. 
        // If the application is running in the foreground, the toast notification is instead routed to the application.
        void notificationChanel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            string relativeUri = string.Empty;

            if (e.Collection != null)
            {
                Dictionary<string, string> collection = (Dictionary<string, string>)e.Collection;
                System.Text.StringBuilder messageBuilder = new System.Text.StringBuilder();

                if (collection.ContainsKey("wp:Param"))
                {
                    if (NumberPush != null)
                        NumberPush(collection["wp:Param"]);
                }


            }


        }

        // Receiving a raw notification. 
        // Raw notifications are only delivered to the application when it is running in the foreground. 
        // If the application is not running in the foreground, the raw notification message 
        // is dropped on the Push Notification Service and is not delivered to the device.
        void notificationChanel_HttpNotificationReceived(object sender, HttpNotificationEventArgs e)
        {
            if (e.Notification.Body != null && e.Notification.Headers != null)
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(e.Notification.Body);
            }
        }

        // Binding a notification channel to a toast notification.
        private void BindingANotificationsChannelToAToastNotification()
        {
            if (!notificationChanel.IsShellToastBound)
            {
                notificationChanel.BindToShellToast();
            }
        }
    }
}
